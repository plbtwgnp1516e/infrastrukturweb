<?php
Class Feed_Model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function insert_user($data)
	{
		$this->db->insert('feedback',$data);
	}

	function select_all()
	{
		$this->db->select('*');
		$this->db->from('feedback');

		return $this->db->get();
	}

	function select_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('feedback');
		$this->db->where('id',$id);

		return $this->db->get();
	}

	function delete_data($id){
		$this->db->where('id',$id);
		$this->db->delete('feedback');
	}

	/*
	function UpdateDataUser($nama, $username, $pass, $id)
	{
		$data['nama']=$nama;
    $data['username']=$username;
    $data['pass']=$pass;

		$this->db->where('id',$id);
		$this->db->update('user',$data);
	}

	function UpdateDataUserWithPhoto($nama, $username, $pass, $foto, $id)
	{
		$data['nama']=$nama;
    $data['username']=$username;
    $data['pass']=$pass;
		$data['foto']=$foto;

		$this->db->where('id',$id);
		$this->db->update('user',$data);
	}*/


}
?>
