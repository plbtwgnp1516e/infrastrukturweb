<!DOCTYPE html>
<html>
<head>
<title>Infrastruktur</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Combatant Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="<?php echo base_url("assets/infrastruktur/frontend/css/bootstrap.css");?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url("assets/infrastruktur/frontend/css/style.css");?>" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script src="<?php echo base_url("assets/infrastruktur/frontend/js/jquery-1.11.1.min.js");?>"></script>
<!-- //js -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="<?php echo base_url("assets/infrastruktur/frontend/js/move-top.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/infrastruktur/frontend/js/easing.js");?>"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>

<body>
			<!-- nav -->
				<div class="navigation">
					<nav class="navbar navbar-default">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
							<nav class="cl-effect-20" id="cl-effect-20">
								<ul class="nav navbar-nav">
									<li><a href="<?php echo base_url()?>"><span data-hover="Home">Home</span></a></li>
									<li><a href="<?php echo base_url('api_page')?>"><span data-hover="API">API</span></a></li>
									<li class="active"><a href="<?php echo base_url('aboutus')?>"><span data-hover="About Us">About Us</span></a></li>
								</ul>
							</nav>
						</div>
						<!-- /.navbar-collapse -->
					</nav>
				</div>
			<!-- //nav -->
			<div style="height:340px">
			<div id="clouds">
				<div class="cloud x1"></div>
				<div class="cloud x3"></div>
				<div class="cloud x4"></div>
				<div class="cloud x5"></div>
			</div>
			</div>
				<div class="information">
						<!-- <button class="enjoy-css" onclick="add_person()"><p style="font-size:16pt; color:#ffffff;">Join Us!</p></button> -->
						<p><h1>
							<strong>Tentang Infrastruktur</strong></h1>
							<br>
							Infrastruktur merupakan aplikasi yang bertujuan untuk menghimpun data infrastruktur yang ada di seluruh daerah indonesia.
							Tidak hanya itu, dengan adanya aplikasi ini diharapkan pengguna dapat memberikan keluhan dan saran atas infrastruktur atau kejadian
							yang terjadi di sekitarnya.
							<br><br><br><br><br><br>
						</p>
				</div>
			<!-- //banner -->
<!-- //body-content -->
<!-- footer -->

<!-- //footer -->
<!-- for bootstrap working -->
	<script src="<?php echo base_url("assets/infrastruktur/frontend/js/bootstrap.js");?>"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->

<!-- //here ends scrolling icon -->
<script src="<?php echo base_url('assets/jquery/jquery-2.1.4.min.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/DataTables/js/jquery.dataTables.min.js')?>"></script>
<script src="<?php echo base_url('assets/DataTables/js/dataTables.bootstrap.js')?>"></script>
<script src="<?php echo base_url('assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script type="text/javascript">
function add_person()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Join Us'); // Set Title to Bootstrap modal title
}
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable
    var url = "<?php echo site_url('frontend/add')?>";
    // ajax adding data to database
		$.ajax({
					 url : "<?php echo site_url('frontend/add')?>",
					 type: "POST",
					 data: $('#form').serialize(),
					 dataType: "JSON",
					 success: function(data)
					 {
						 if(data.status){
							 $('#modal_form').modal('hide');
							 alert('Anda berhasil terdaftar di sistem infrastruktur! silahkan download aplikasinya di Google Playstore.\nAPI KEY anda : '+data.random);
							 $('#btnSave').text('save');
					     $('#btnSave').attr('disabled',false);
						 }
						 if(!data.status)
						 {
							 alert('Error : '+data.error);
							 $('#btnSave').text('save');
					     $('#btnSave').attr('disabled',false);
						 }

					 },
					 error: function (jqXHR, textStatus, errorThrown)
					 {
							 alert(textStatus);
					 }
			 });
}
</script>
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Sign Up Form</h3>
            </div>
            <div class="modal-body form">
                <form action="frontend/add" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Username</label>
                            <div class="col-md-9">
                                <input name="username" placeholder="username" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Password</label>
                            <div class="col-md-9">
                                <input name="password" placeholder="password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Conf. Password</label>
                            <div class="col-md-9">
                                <input name="confpassword" placeholder="confrirmation password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>
												<div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="example@example.com" class="form-control" type="email">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
</body>
</html>
