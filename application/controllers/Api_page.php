<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_page extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('Key_Model','',TRUE);
 }

 function index()
 {
   $this->load->helper(array('form'));
   $this->load->view('api_view');
   $this->load->helper('url');
   $this->load->helper(array('form', 'url'));
 }

}

?>
