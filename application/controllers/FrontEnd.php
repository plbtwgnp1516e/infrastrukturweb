<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FrontEnd extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('Key_Model','',TRUE);
 }

 function index()
 {
   $this->load->helper(array('form'));
   $this->load->view('main_view');
   $this->load->helper('url');
   $this->load->helper(array('form', 'url'));
 }

 public function add(){
   $username=$this->input->post('username');
   $password=$this->input->post('password');
   $confpass=$this->input->post('confpassword');
   $email=$this->input->post('email');
   $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);

   if($username==NULL||$password==NULL||$confpass==NULL||$email==NULL){
     echo json_encode(array("status" => FALSE,"error"=>"Field tidak boleh kosong!"));
   }elseif(!($password==$confpass))
   {
     echo json_encode(array("status" => FALSE,"error"=>"Password konfirmasi tidak sesuai!"));
   }elseif(strlen($username)<8||strlen($password)<7){
     echo json_encode(array("status" => FALSE,"error"=>"Username / Password minimal 8 karakter!"));
   }else{
     $this->Key_Model->add_user($username, $password,$email);
     $this->Key_Model->add($randomString,$username, $password,$email);
     echo json_encode(array("status" => TRUE,"random"=>$randomString,"username"=>$username));
   }
 }
public function cek(){
   $username=$this->input->post('username');
   $password=$this->input->post('password');
   $password=md5($password);
   $apikey=$this->Key_Model->cekkey($username,$password);
   echo json_encode(array("status" => TRUE,"key"=>$apikey[0]->key));
 }

}

?>
