<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Keluhan extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['keluhan_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['keluhan_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['keluhan_delete']['limit'] = 50; // 50 requests per hour per user/key
    		$this->load->helper('url');
    		$this->load->helper(array('form', 'url'));
        $this->load->model('Keluhan_Model','',TRUE);
    }

  public function get_get(){
		$id = $this->uri->segment(4);
    if(isset($id))
    {
        $data =  $this->Keluhan_Model->get_by(array('id'=>$id));
        if(isset($data['id'])){
          $this->response(array('status'=>'success','message'=>$data));
        }
        else{
          $this->response(array('status'=>'failure','message'=>'The specified data could not be found'),REST_Controller::HTTP_NOT_FOUND);
        }
    }else{
      $data =  $this->Keluhan_Model->getdata();
      $this->response(array('status'=>'success','message'=>$data));
    }
	}

  public function insert_post(){
    $this->load->library('form_validation');
    $this->form_validation->set_data($this->post());
    if($this->form_validation->run('keluhan_put')!=false){
      $img=$this->post('photo_path');
      $decode_img = base64_decode($img);
      $photo_name=substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
      file_put_contents('assets/uploads/files/'.$photo_name.'.jpg', $decode_img);
      $data=$this->post();
      $data['photo_path']=$photo_name.".jpg"; 
      date_default_timezone_set("Asia/Jakarta");
      $date = date("Y-m-d  h:i:sa");
      $databaru=array('nama'=>$data['nama'],'lat'=>$data['lat'],'longitude'=>$data['longitude'],'keluhan'=>$data['keluhan'],'photo_path'=>$data['photo_path'],'kategori'=>$data['kategori'],'tanggal'=>$date,'id_pelapor'=>$data['id_pelapor']);     
      $id=$this->Keluhan_Model->insert($databaru);
      if(!$id){
        $this->response(array('status'=>'failure','message'=>'An unexpected error while trying to create the data'),REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
      }else{
        
        $this->load->model('User_Model','',TRUE);
        $data_user =  $this->User_Model->get_by(array('id'=>$data['id_pelapor']));
        $poin_baru =$data_user['point']+300;
        $this->User_Model->update($data_user['id'],array('point'=>$poin_baru));
        $this->response(array('status'=>'success','message'=>'Data Created'));
      }
    }else{
      $this->response(array('status'=>'failure','message'=>$this->form_validation->get_errors_as_array()),REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function post_post()
  {
    $id = $this->uri->segment(4);
    if(isset($id))
    {
        $data =  $this->Keluhan_Model->get_by(array('id'=>$id));
        if(isset($data['id'])){
          $this->load->library('form_validation');
          $this->form_validation->set_data($this->post());
          if($this->form_validation->run('keluhan_post')!=false){
            $img=$this->post('photo_path');
            if($img!=null){
            $decode_img = base64_decode($img);
            $photo_name=substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
            file_put_contents('assets/uploads/files/'.$photo_name.'.jpg', $decode_img);
            $data=$this->post();
            $data['photo_path']=$photo_name.".jpg";}else{
               $data=$this->post();
            }
            $updated=$this->Keluhan_Model->update($id,$data);
            if(!$updated){
              $this->response(array('status'=>'failure','message'=>'An unexpected error while trying to update the data'),REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }else{
              $this->response(array('status'=>'success','message'=>'Data Edited'));
            }
          }else{
            $this->response(array('status'=>'failure','message'=>$this->form_validation->get_errors_as_array()),REST_Controller::HTTP_BAD_REQUEST);
          }
        }
        else{
          $this->response(array('status'=>'failure','message'=>'The specified data could not be found'),REST_Controller::HTTP_NOT_FOUND);
        }
    }else{
      $this->response(array('status'=>'failure','message'=>'You must enter the data id!'),REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function delete_delete(){
		$id = $this->uri->segment(4);
    if(isset($id))
    {
        $data =  $this->Keluhan_Model->get_by(array('id'=>$id));
        if(isset($data['id'])){
          $deleted=$this->Keluhan_Model->delete($id,$data);
          if(!$deleted)
          {
            $this->response(array('status'=>'failure','message'=>'An unexpected error while trying to delete the data'),REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
          }else{
            $this->response(array('status'=>'success','message'=>'Data Deleted'));
          }
        }
        else{
          $this->response(array('status'=>'failure','message'=>'The specified data could not be found'),REST_Controller::HTTP_NOT_FOUND);
        }
    }else{
      $this->response(array('status'=>'failure','message'=>'You must enter the data id!'),REST_Controller::HTTP_BAD_REQUEST);
    }
	}

}
