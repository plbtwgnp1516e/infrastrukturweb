<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class User extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    		$this->load->helper('url');
    		$this->load->helper(array('form', 'url'));
        $this->load->model('User_Model','',TRUE);
    }

  public function get_get(){
		$id = $this->uri->segment(4);
    if(isset($id))
    {
        $data =  $this->User_Model->get_by(array('id'=>$id));
        if(isset($data['id'])){
          $this->response(array('status'=>'success','message'=>$data));
        }
        else{
          $this->response(array('status'=>'failure','message'=>'The specified data could not be found'),REST_Controller::HTTP_NOT_FOUND);
        }
    }else{
      $data =  $this->User_Model->get_all();
      $this->response(array('status'=>'success','message'=>$data));
    }
	}
  public function getbyusername_get(){
		$un = $this->uri->segment(4);
                $data =  $this->User_Model->get_by(array('username'=>$un));
       
        if(isset($data['id'])){
          $this->response(array('status'=>'success','message'=>$data));
        }
        else{
          $this->response(array('status'=>'failure','message'=>'The specified data could not be found'),REST_Controller::HTTP_NOT_FOUND);
        }
	}

  public function insert_post(){
    $this->load->library('form_validation');
    $this->form_validation->set_data($this->post());
    if($this->form_validation->run('user_put')!=false){
      $exist = $this->User_Model->get_by(array('email'=>$this->put('email')));
      if($exist)
      {
        $this->response(array('status'=>'failure','message'=>'The specified email address is already exist'),REST_Controller::HTTP_CONFLICT);
      }
      $img=$this->post('photo_path');
      $decode_img = base64_decode($img);
      $photo_name=substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
      file_put_contents('assets/uploads/files/'.$photo_name.'.jpg', $decode_img);
      $data=$this->post();
      $data['photo_path']=$photo_name.".jpg";
      $id=$this->User_Model->insert($data);
      if(!$id){
        $this->response(array('status'=>'failure','message'=>'An unexpected error while trying to create the data'),REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
      }else{
        $this->response(array('status'=>'success','message'=>'Data Created'));
      }
    }else{
      $this->response(array('status'=>'failure','message'=>$this->form_validation->get_errors_as_array()),REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function post_post()
  {
    $id = $this->uri->segment(4);
    if(isset($id))
    {
        $data =  $this->User_Model->get_by(array('id'=>$id));
        if(isset($data['id'])){
          $this->load->library('form_validation');
          $this->form_validation->set_data($this->post());
          if($this->form_validation->run('user_post')!=false){
            $img=$this->post('photo_path');
            if($img!=null){
            $decode_img = base64_decode($img);
            $photo_name=substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
            file_put_contents('assets/uploads/files/'.$photo_name.'.jpg', $decode_img);
            $data=$this->post();
            $data['photo_path']=$photo_name.".jpg";}else{
               $data=$this->post();
               
            }
            $data['password']=md5($data['password']);
            $updated=$this->User_Model->update($id,$data);
            if(!$updated){
              $this->response(array('status'=>'failure','message'=>'An unexpected error while trying to update the data'),REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }else{
              $this->response(array('status'=>'success','message'=>'Data Edited'));
            }
          }else{
            $this->response(array('status'=>'failure','message'=>$this->form_validation->get_errors_as_array()),REST_Controller::HTTP_BAD_REQUEST);
          }
        }
        else{
          $this->response(array('status'=>'failure','message'=>'The specified data could not be found'),REST_Controller::HTTP_NOT_FOUND);
        }
    }else{
      $this->response(array('status'=>'failure','message'=>'You must enter the data id!'),REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function delete_delete(){
		$id = $this->uri->segment(4);
    if(isset($id))
    {
        $data =  $this->User_Model->get_by(array('id'=>$id));
        if(isset($data['id'])){
          $deleted=$this->User_Model->delete($id,$data);
          if(!$deleted)
          {
            $this->response(array('status'=>'failure','message'=>'An unexpected error while trying to delete the data'),REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
          }else{
            $this->response(array('status'=>'success','message'=>'Data Deleted'));
          }
        }
        else{
          $this->response(array('status'=>'failure','message'=>'The specified data could not be found'),REST_Controller::HTTP_NOT_FOUND);
        }
    }else{
      $this->response(array('status'=>'failure','message'=>'You must enter the data id!'),REST_Controller::HTTP_BAD_REQUEST);
    }
	}

  public function login_get(){
    $username = $this->uri->segment(4);
    $password = $this->uri->segment(5);
    $data=$this->User_Model->login($username,$password);
    $id=$this->User_Model->getid($username,$password);
    if($data!=null){
      $this->response(array('status'=>'success','message'=>$id));
    }
    else {
      $this->response(array('status'=>'failure','message'=>'Login failure'));
    }

  }

}
