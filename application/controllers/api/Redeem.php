<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Redeem extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    		$this->load->helper('url');
    		$this->load->helper(array('form', 'url'));
        $this->load->model('History_Model','',TRUE);
    }

  public function history_get(){
		$id = $this->uri->segment(4);
   
      $data =  $this->History_Model->get_data($id);
      if($data==null){
         $this->response(array('status'=>'success','message'=>'no data with specified id'));
      }else{
        $this->response(array('status'=>'success','message'=>$data));
      }
   }

  public function doredeem_post(){
    $id_user=$this->post('id_user');
    $id_barang=$this->post('id_barang');
    $this->load->model('Reward_Model','',TRUE);
    $this->load->model('User_Model','',TRUE);

    $data_user =  $this->User_Model->get_by(array('id'=>$id_user));
    $data_barang =  $this->Reward_Model->get_by(array('id'=>$id_barang));

    if($data_user['point']<$data_barang['point']){
        $this->response(array('status'=>'failure','message'=>'Your point is not enough'));
    }else{
      $redeem_code=substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 20);
      date_default_timezone_set("Asia/Jakarta");
      $date = date("Y-m-d  h:i:sa");

      $history = array('id_user' =>$id_user ,'nama_barang'=>$data_barang['nama'],'photo_path'=>$data_barang['photo_path'], 'tanggal'=>$date, 'redeem_code'=>$redeem_code );
      $id=$this->History_Model->insert($history);
      $point_baru=$data_user['point']-$data_barang['point'];
      $stok_baru=(int)$data_barang['stok']-1;
      $update=$this->User_Model->update($id_user,array('point'=>$point_baru));
      $updatestok=$this->Reward_Model->update($id_barang,array('stok'=>$stok_baru));
      if(!$id||!$update||!$updatestok){
          $this->response(array('status'=>'failure','message'=>'An unexpected error while trying to process the data'),REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
      }else{
          $this->response(array('status'=>'success','message'=>'Your redeem code is '.$redeem_code));
      }

    }

  }



}
