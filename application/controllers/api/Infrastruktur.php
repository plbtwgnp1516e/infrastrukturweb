<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Infrastruktur extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    		$this->load->helper('url');
    		$this->load->helper(array('form', 'url'));
    		$this->load->model('inf_model','',TRUE);
        $this->load->model('feed_model','',TRUE);
        $this->load->model('news_model','',TRUE);
        $this->load->model('poin_model','',TRUE);
        $this->load->model('reward_model','',TRUE);
        $this->load->model('user_model','',TRUE);
    }

	public function data_get(){
		$data['daftar_infrastruktur']=$this->inf_model->select_all()->result();
    $id = $this->get('id');
		//$this->response($data, REST_Controller::HTTP_OK);
    // If the id parameter doesn't exist return all the users

    if ($id === NULL)
    {
        // Check if the users data store contains users (in case the database result returns NULL)
        if ($data)
        {
            // Set the response and exit
            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No users were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
    if($id != NULL)
    {
        $data['daftar_infrastruktur']=$this->inf_model->select_by_id($id)->result();
        if ($data)
        {
            // Set the response and exit
            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No users were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
	}

  public function data_put(){
      $this->inf_model->insert_data($this->put());
      $data = $this->put();
      $this->response($data['nama'], REST_Controller::HTTP_OK);
  }

  public function feedback_get(){
		$data['daftar_feedback']=$this->feed_model->select_all()->result();
    $id = $this->get('id');
		//$this->response($data, REST_Controller::HTTP_OK);
    // If the id parameter doesn't exist return all the users

    if ($id === NULL)
    {
        // Check if the users data store contains users (in case the database result returns NULL)
        if ($data)
        {
            // Set the response and exit
            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No users were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
    if($id != NULL)
    {
        $data['daftar_feedback']=$this->feed_model->select_by_id($id)->result();
        if ($data)
        {
            // Set the response and exit
            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No users were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
	}

  public function feedback_put(){
      $this->feed_model->insert_data($this->put());
      $this->response($this->put(), REST_Controller::HTTP_OK);
  }

  public function news_get(){
		$data['daftar_newsfeed']=$this->news_model->select_all()->result();
    $id = $this->get('id');
		//$this->response($data, REST_Controller::HTTP_OK);
    // If the id parameter doesn't exist return all the users

    if ($id === NULL)
    {
        // Check if the users data store contains users (in case the database result returns NULL)
        if ($data)
        {
            // Set the response and exit
            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No users were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
    if($id != NULL)
    {
        $data['daftar_newsfeed']=$this->news_model->select_by_id($id)->result();
        if ($data)
        {
            // Set the response and exit
            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No users were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
	}
  public function news_put(){
      $this->news_model->insert_data($this->put());
      $this->response($this->put(), REST_Controller::HTTP_OK);
  }

  public function poin_get(){
		$data['point']=$this->poin_model->select_all()->result();
    $id = $this->get('id');
		//$this->response($data, REST_Controller::HTTP_OK);
    // If the id parameter doesn't exist return all the users

    if ($id === NULL)
    {
        // Check if the users data store contains users (in case the database result returns NULL)
        if ($data)
        {
            // Set the response and exit
            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No users were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
    if($id != NULL)
    {
        $data['point']=$this->poin_model->select_by_id($id)->result();
        if ($data)
        {
            // Set the response and exit
            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No users were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
	}

  public function reward_get(){
		$data['reward']=$this->reward_model->select_all()->result();
    $id = $this->get('id');
		//$this->response($data, REST_Controller::HTTP_OK);
    // If the id parameter doesn't exist return all the users

    if ($id === NULL)
    {
        // Check if the users data store contains users (in case the database result returns NULL)
        if ($data)
        {
            // Set the response and exit
            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No users were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
    if($id != NULL)
    {
        $data['reward']=$this->reward_model->select_by_id($id)->result();
        if ($data)
        {
            // Set the response and exit
            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No users were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
	}

  public function user_get(){
		$id = $this->uri->segment(4);
    if(isset($id))
    {
        $data =  $this->user_model->get_by(array('id'=>$id));
        if(isset($data['id'])){
          $this->response(array('status'=>'success','message'=>$data));
        }
        else{
          $this->response(array('status'=>'failure','message'=>'The specified data could not be found'),REST_Controller::HTTP_NOT_FOUND);
        }
    }else{
      $data =  $this->user_model->get_all();
      $this->response(array('status'=>'success','message'=>$data));
    }
	}

  public function user_put(){
    $this->load->library('form_validation');
    $this->form_validation->set_data($this->put());
    if($this->form_validation->run('user_put')!=false){
      $exist = $this->user_model->get_by(array('email'=>$this->put('email')));
      if($exist)
      {
        $this->response(array('status'=>'failure','message'=>'The specified email address is already exist'),REST_Controller::HTTP_CONFLICT);
      }
      $data=$this->put();
      $id=$this->user_model->insert($data);
      if(!$id){
        $this->response(array('status'=>'failure','message'=>'An unexpected error while trying to create the data'),REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
      }else{
        $this->response(array('status'=>'success','message'=>'Data Created'));
      }
    }else{
      $this->response(array('status'=>'failure','message'=>$this->form_validation->get_errors_as_array()),REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function user_post()
  {
    $id = $this->uri->segment(4);
    if(isset($id))
    {
        $data =  $this->user_model->get_by(array('id'=>$id));
        if(isset($data['id'])){
          $this->load->library('form_validation');
          $this->form_validation->set_data($this->post());
          if($this->form_validation->run('user_post')!=false){
            $data=$this->post();
            $updated=$this->user_model->update($id,$data);
            if(!$updated){
              $this->response(array('status'=>'failure','message'=>'An unexpected error while trying to update the data'),REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
            }else{
              $this->response(array('status'=>'success','message'=>'Data Edited'));
            }
          }else{
            $this->response(array('status'=>'failure','message'=>$this->form_validation->get_errors_as_array()),REST_Controller::HTTP_BAD_REQUEST);
          }
        }
        else{
          $this->response(array('status'=>'failure','message'=>'The specified data could not be found'),REST_Controller::HTTP_NOT_FOUND);
        }
    }else{
      $this->response(array('status'=>'failure','message'=>'You must enter the data id!'),REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function user_delete(){
		$id = $this->uri->segment(4);
    if(isset($id))
    {
        $data =  $this->user_model->get_by(array('id'=>$id));
        if(isset($data['id'])){
          $deleted=$this->user_model->delete($id,$data);
          if(!$deleted)
          {
            $this->response(array('status'=>'failure','message'=>'An unexpected error while trying to delete the data'),REST_Controller::HTTP_INTERNAL_SERVER_ERROR);
          }else{
            $this->response(array('status'=>'success','message'=>'Data Deleted'));
          }
        }
        else{
          $this->response(array('status'=>'failure','message'=>'The specified data could not be found'),REST_Controller::HTTP_NOT_FOUND);
        }
    }else{
      $this->response(array('status'=>'failure','message'=>'You must enter the data id!'),REST_Controller::HTTP_BAD_REQUEST);
    }
	}

}
