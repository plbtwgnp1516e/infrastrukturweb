<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        /* Standard Libraries of codeigniter are required */
        $this->load->database();
        $this->load->helper('url');
        /* ------------------ */

        $this->load->library('grocery_CRUD');

    }

    public function index()
    {
      if($this->session->userdata('logged_in'))
      {
        $this->data();
      }
      else
      {
        //If no session, redirect to login page
        redirect('login', 'refresh');
      }
    }

    public function data()
    {
        $this->grocery_crud->set_theme('datatables');
        $this->grocery_crud->set_language('indonesian');
        $this->grocery_crud->set_table('data')
              ->columns('nama','keluhan','photo_path','tanggal','id_pelapor');
        

        $this->grocery_crud->set_field_upload('photo_path','assets/uploads/files');
        $this->grocery_crud->set_relation('id_pelapor','user','username');
        $this->grocery_crud->set_rules('lat','Latitude','numeric');
        $this->grocery_crud->set_rules('longitude','Longitude','numeric');
        $state = $this->grocery_crud->getState();
        $this->grocery_crud->where('status','');
        $this->grocery_crud->add_action('Konfirmasi', '', 'main/konfirmasi','ui-icon-plus');
        $this->grocery_crud->unset_edit();
        $this->grocery_crud->unset_add();

        $this->grocery_crud->field_type('tanggal','datetime');
        $output = $this->grocery_crud->render();

        $this->_example_output($output);
    }

    function konfirmasi($id)
    {

      $this->db->query('UPDATE `user` SET `point`=`point`+500 WHERE id=(SELECT `id_pelapor` FROM `data` WHERE `id`='.$id.')');
      $this->db->query('UPDATE `data` SET `status`="Terpercaya" WHERE id='.$id.'');
      redirect('Main','refresh');
      die();
    }

    function _example_output($output = null)
    {
        $this->load->view('our_template.php',$output);
    }
}
