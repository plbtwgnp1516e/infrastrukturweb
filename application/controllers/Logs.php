<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logs extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        /* Standard Libraries of codeigniter are required */
        $this->load->database();
        $this->load->helper('url');
        /* ------------------ */

        $this->load->library('grocery_CRUD');

    }

    public function index()
    {
      if($this->session->userdata('logged_in'))
      {
        $this->data();
      }
      else
      {
        //If no session, redirect to login page
        redirect('login', 'refresh');
      }
    }

    public function data()
    {
        $this->grocery_crud->set_theme('datatables');
        $this->grocery_crud->set_language('indonesian');
        $this->grocery_crud->set_table('logs')->columns('uri','method','id_address','time','rtime','response_code');
        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_edit();
        $this->grocery_crud->unset_delete();
        
        $output = $this->grocery_crud->render();

        $this->_example_output($output);
    }

    function _example_output($output = null)
    {
        $this->load->view('logs_view.php',$output);
    }
}
