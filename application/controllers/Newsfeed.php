<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsfeed extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        /* Standard Libraries of codeigniter are required */
        $this->load->database();
        $this->load->helper('url');
        /* ------------------ */

        $this->load->library('grocery_CRUD');

    }

    public function index()
    {
      if($this->session->userdata('logged_in'))
      {
        $this->data();
      }
      else
      {
        //If no session, redirect to login page
        redirect('login', 'refresh');
      }
    }

    public function data()
    {
        $this->grocery_crud->set_theme('datatables');
        $this->grocery_crud->set_language('indonesian');
        $this->grocery_crud->set_table('newsfeed');
        $this->grocery_crud->set_field_upload('photo_path','assets/uploads/files');
        $this->grocery_crud->field_type('tanggal','datetime');
        $output = $this->grocery_crud->render();

        $this->_example_output($output);
    }

    function _example_output($output = null)
    {
        $this->load->view('newsfeed_view.php',$output);
    }
}
