<?php

defined('BASEPATH') OR exit('No direct script access allowed');


$config = array(
  'user_put'=>array(
    array('field'=>'email','label'=>'email','rules'=>'trim|required|valid_email'),
    array('field'=>'username','label'=>'username','rules'=>'trim|required|min_length[4]|max_length[16]'),
    array('field'=>'password','label'=>'password','rules'=>'trim|required|min_length[4]|max_length[16]'),
  ),
  'user_post'=>array(
    array('field'=>'email','label'=>'email','rules'=>'trim|valid_email'),
    array('field'=>'username','label'=>'username','rules'=>'trim|min_length[4]|max_length[16]'),
    array('field'=>'password','label'=>'password','rules'=>'trim|min_length[4]|max_length[16]'),
  ),
  'keluhan_put'=>array(
    array('field'=>'nama','field'=>'nama','rules'=>'trim|required'),
    array('field'=>'lat','field'=>'lat','rules'=>'trim|required|numeric'),
    array('field'=>'longitude','field'=>'longitude','rules'=>'trim|required|numeric'),
    array('field'=>'keluhan','field'=>'keluhan','rules'=>'trim|required'),
    array('field'=>'photo_path','field'=>'photo_path','rules'=>'trim|required'),
    array('field'=>'kategori','field'=>'kategori','rules'=>'trim|required'),
    array('field'=>'id_pelapor','field'=>'id_pelapor','rules'=>'trim|required'),
  ),
  'keluhan_post'=>array(
    array('field'=>'nama','field'=>'nama','rules'=>'trim'),
    array('field'=>'lat','field'=>'lat','rules'=>'trim|numeric'),
    array('field'=>'longitude','field'=>'longitude','rules'=>'trim|numeric'),
    array('field'=>'keluhan','field'=>'keluhan','rules'=>'trim'),
    array('field'=>'photo_path','field'=>'photo_path','rules'=>'trim'),
    array('field'=>'kategori','field'=>'kategori','rules'=>'trim'),
  ),
);

?>
